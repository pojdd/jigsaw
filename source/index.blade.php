@extends('_layouts.master')

@section('content')
    <div class="content">
        <div class="m-b-md">
            <h1 class="title">Jigsaw</h1>

            <p>Static Sites for Laravel Developers</p>
        </div>

        <div class="links">
            <a href="{{ $base }}/about">About</a>
        </div>
    </div>
@endsection
